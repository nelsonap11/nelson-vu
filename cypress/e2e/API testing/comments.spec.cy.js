
//javascript
// cypress/integration/posts.spec.js

var titulo;
var testArry;

//API testing
//Method POST
it('Mehtod - POST',()=>{
    cy.request({
        method: 'POST',
        url:'https://jsonplaceholder.typicode.com/comments',
        body: JSON.stringify({
          title: 'foo',
          body: 'bar',
          userId: 1,
          id: 1
        }),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }).then((response)=>{
        expect(response).property('status').to.equal(201)
        expect(response.body).property('id').to.not.be.oneOf([null, ""])
        expect(response.body.title).to.eql("foo")
        expect(response.body.body).to.eql("bar")
        expect(response.body.userId).to.eql(1)
        expect(response.body.id).to.eql(501)
      }).then((json) => console.log(json));
    })

  //Method GET
    it('Mehtod - GET',()=>{
      cy.request({
          method: 'GET',
          url:'https://jsonplaceholder.typicode.com/comments/1',
          body: JSON.stringify({
            title: 'foo',
            body: 'bar',
            userId: 1,
          }),
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
        }).then((response)=>{
          expect(response).property('status').to.equal(200)
          expect(Cypress._.every(testArry, ["userId", 1])).to.equal(true);
          expect(Cypress._.every(testArry, ["id", 1])).to.equal(true);
          expect(Cypress._.every(testArry, ["title", "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"])).to.equal(true);
          expect(Cypress._.every(testArry, ["quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"])).to.equal(true);
        }).then((json) => console.log(json));
      })


 //Method PUT
      it('Mehtod - PUT',()=>{
        cy.request({
            method: 'PUT',
            url:'https://jsonplaceholder.typicode.com/comments/1',
            body: JSON.stringify({
              title: 'foo',
              body: 'bar',
              userId: 1,
              id: 1
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
          }).then((response)=>{
            expect(response).property('status').to.equal(200)
            expect(response.body).property('id').to.not.be.oneOf([null, ""])
            expect(response.body.title).to.eql("foo")
            expect(response.body.body).to.eql("bar")
            expect(response.body.userId).to.eql(1)
            expect(response.body.id).to.eql(1)
          }).then((json) => console.log(json));
        })

//Method PATCH

        it('Mehtod - PATCH',()=>{
          cy.request({
              method: 'PATCH',
              url:'https://jsonplaceholder.typicode.com/comments/1',
              body: JSON.stringify({
                title: 'foo',
                body: "bar",
                userId: 1,
                id: 1
              }),
              headers: {
                'Content-type': 'application/json; charset=UTF-8',
              },
            }).then((response)=>{
              expect(response).property('status').to.equal(200)
              expect(response.body).property('id').to.not.be.oneOf([null, ""])
              expect(response.body.title).to.eql("foo")
              expect(response.body.body).to.eql("bar")
              expect(response.body.userId).to.eql(1)
              expect(response.body.id).to.eql(1)
              expect(response.body.email).to.eql("Eliseo@gardner.biz")
              expect(response.body.name).to.eql("id labore ex et quam laborum")
              expect(response.body.postId).to.eql(1)
            }).then((json) => console.log(""));
          })


//Method DELETE

        it('Mehtod - DELETE',()=>{
          cy.request({
              method: 'DELETE',
              url:'https://jsonplaceholder.typicode.com/comments/1',
              body: JSON.stringify({
              }),
            }).then((response)=>{
              expect(response).property('status').to.equal(200)
            }).then((json) => console.log(""));
          })
