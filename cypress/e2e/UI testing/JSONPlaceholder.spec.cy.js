/// <reference types="cypress" />

describe('example to-do app', () => {
  beforeEach(() => {
  })
})
it('cy.visit() - JSON Placeholder', () => {
cy.wait(5000)
cy.visit('https://jsonplaceholder.typicode.com/guide', {


  })
  cy.contains('Getting a resource').click()
  cy.wait(2000)

  cy.contains('Listing all resources').click()
  cy.wait(2000)
  cy.contains('Creating a resource').click()
  cy.wait(2000)
  cy.contains('Updating a resource').click()
  cy.wait(2000)
  cy.contains('Patching a resource').click()
  cy.wait(2000)
  cy.contains('Deleting a resource').click()
  cy.wait(2000)
  cy.contains('Filtering resources').click()
  cy.wait(2000)
  cy.contains('Listing nested resources').click()
  })

